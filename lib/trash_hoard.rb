#!/usr/bin/env ruby

# FLAW: item numbers as numbers: 001 

require 'parse-ruby-client'
require 'awesome_print'

class TrashHoard
  attr_accessor :array

  Parse.init :application_id => "VnL3omWq0yue4phAuJsRAQgsWamxvTCNxe6cET77",
             :api_key        => "KsN3okFez2oE5pRHElsKeAa0SFXztwGs7ZSLb6BU"

  def self.load_from_file
    own_txt = File.dirname(__FILE__) + "/../data/own.txt"
    puts "Load from #{own_txt}"

    own_list = File.open(own_txt).read.split.map{|m| m.to_i}
    puts "#{own_list.count} Own Trashies"
    ap own_list
    
    own_hash = Hash.new(0)
    own_list.each do |item|
      own_hash[item] += 1
    end
    
    own_hash.each do |item, count|
      own = Parse::Object.new "own", { :item => item, :count => count }
      own.save
    end

    ap Parse.get "own"
  end

  def initialize
    refresh
  end

  def refresh
    @array = Parse.get "own"
  end

  def unique
    @array.count
  end

  def add(item, num=1)
    res = Parse::Query.new("own").eq("item", item).get
    if res.empty?
      own = Parse::Object.new "own", { :item => item, :count => 1 }
      own.save
    else
      own = res[0]
      own['count'] += 1
      own.save
    end
    refresh
  end

  def minus(item, num=1)
    raise "Not Implemented"
  end
  
  def count
    total = 0
    @array.each do |hash|
      total += hash['count']
    end
    total
  end

  def list
    list = []
    @array.each do |hash|
      hash['count'].times do |count|
        list << hash['item']
      end
    end
    list.sort
  end

end

# metadata = JSON.load(File.read(File.dirname(__FILE__)+"/../data/trashpile/METADATA.json"))
# metadata.each do |meta|
#   metadata = Parse::Object.new "metadata", meta
#   metadata.save
# end


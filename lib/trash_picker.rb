#!/usr/bin/env ruby

$: << File.dirname(__FILE__) + "/../lib"
require 'trash_hoard'

require 'json'
require 'awesome_print'

class TrashPicker
  attr_accessor :metadata
  attr_accessor :metahash
  attr_accessor :own_list

  def initialize(dir='public/trashpile')
    @metadata = []
    @metahash = {}

    puts "Reading Metafile"
    @metadata = JSON.load(File.read("#{dir}/METADATA.json"))
    puts "#{@metadata.count} Total Trashies"

    puts "Reading Own List"
    #own_txt = File.dirname(__FILE__) + "/../data/own.txt"
    #@own_list = File.open(own_txt).read.split.map{|m| m.to_i}
    @own_list = TrashHoard.new.list
    puts "#{@own_list.count} Own Trashies"

    @metadata.each do |metadata|
      @metahash[metadata['item_num']] = metadata
      @metahash[metadata['item_num']]['own'] = @own_list.include? metadata['item_num']
    end
  end

  #
  # Counts
  #

  def count 
    @metadata.count
  end

  def counts
    {:image_files => @image_files.count, :meta_files => @meta_files.count}
  end
  
  #
  # All
  #
  
  def all_count
    @metahash.count
  end

  def all
    @metahash
  end
  
  #
  # Own
  #
  
  def own_count
    @own_list = TrashHoard.new.list
    @own_list.count
  end

  def own
    @own_list = TrashHoard.new.list
    prev_num = nil
    prev_count = 1
    @own_hash = {}
    @own_list.each_with_index do |own_num, index|
      key = own_num
      if own_num == prev_num
        prev_count += 1
        key = "#{own_num}-#{prev_count}"
      else
        prev_count = 1
      end
      @own_hash[key] = @metahash[own_num]
      prev_num = own_num
    end
    @own_hash
  end

  #
  # Item Index Lookup
  #

  def [](item)
    @metahash[item.to_i]
  end

  #
  # Name
  #

  def name_list
    result = Hash.new(0)
    @metahash.each do |k,v|
      result[v['name']] += 1
    end
    result
  end

  #
  # Team
  #

  def item_list
    result = Hash.new(0)
    @metahash.each do |k,v|
      result[v['item']] += 1
    end
    result.keys
  end

  def team_list
    result = Hash.new(0)
    @metahash.each do |k,v|
      result[v['team'].downcase] += 1
    end
    result
  end

  def team(name)
    @metahash.select { |k,v| v['team'].downcase == name.downcase }
  end

  # 
  # Series
  #

  def series_list
    result = Hash.new(0)
    @metahash.each do |k,v|
      result[v['series']] += 1
    end
    result
  end

  def series(n)
    @metahash.select { |k,v| v['series_num'] == n }
  end

  #
  # Finish
  #

  def finish_list
    result = Hash.new(0)
    @metahash.each do |k,v|
      result[v['finish'].downcase] += 1
    end
    result
  end

  #
  # Rarity
  #

  def rarity_list
    result = Hash.new(0)
    @metahash.each do |k,v|
      result[v['rarity'].downcase] += 1
    end
    result
  end

end

if $0 == __FILE__


tp = TrashPile.new
tp.own_file = "own.txt"

ap tp.all_count
ap tp.all
exit

ap tp.own_count
ap tp.own_list
ap tp.own

ap tp.series(3)

ap tp.team_list
ap tp.series_list
ap tp.name_list
ap tp.item_list
ap tp.finish_list
ap tp.rarity_list

end

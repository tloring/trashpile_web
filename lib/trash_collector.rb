#!/usr/bin/env ruby
#
# TrashCollector - Collects Trashies from Product Website save to "trashpile"
#

$: << File.dirname(__FILE__) + "/../lib"
require 'trashie'

require 'awesome_print'
require 'fileutils'
require 'json'

class TrashCollector

  attr_accessor :metadata

  FIRST_ID = 1
  LAST_ID = 850 # max product id = 850
  SAVE_PATH = "./trashpile"

  def self.collect(first=FIRST_ID, last=LAST_ID, save_path=SAVE_PATH)
    self.new(first, last, save_path)
  end

  def initialize(first, last, save_path)
    puts "Collecting Trashies from #{first}-#{last} to #{save_path}"

    # make sure trashpile exists
    unless File.exist?(save_path)
      FileUtils.mkdir(save_path)
    end

    #
    # collect trashies images and meta data, save images to trashpile
    #
    @metadata = []
    (first..last).each_with_index do |id, index|
      print "[#{index+1}] "

      # get trashie by id
      trashie = Trashie.new(id, save_path)

      # could be series (trashies) or collection 1 (cards)
      if trashie.is_series?
        @metadata << trashie.meta 
        trashie.save_image!
      else
        puts "#{id} Not A Series, Not Saving"
      end
    end

    #
    # save metadata to trashpile
    #
    begin 
      File.open("#{save_path}/METADATA.json", "w") do |f|
        f.write to_json
      end
      puts self
    rescue StandardError => e
      puts e
    end
  end

  def to_s
    to_json
  end

  def to_json
    JSON.pretty_generate(@metadata)
  end

end

#
# Test Harness
#

if $0 == __FILE__

  collection = TrashCollector.collect 1, 2

  ap collection.metadata
  puts collection.to_json

end

#!/usr/bin/env ruby

require 'nokogiri'
require 'awesome_print'

require 'open-uri'
require 'json'

class Trashie
  attr_accessor :meta
  attr_accessor :image

  URL_BASE = "http://collect.trashpack.com"

  def initialize(id=nil, save_path="trashpile")
    # id = nil is for testing purposes, pull html snippet from end of this file

    @meta = {}
    @meta[:id] = id
    @meta[:url_product] = URL_BASE + "/product-modal.aspx?id=#{id}"

    begin
      html_doc = id.nil? ? Nokogiri::HTML(DATA) : Nokogiri::HTML(open(@meta[:url_product]))
    rescue 
      puts "open url fail on #{url}"
    end

    begin 
      @meta[:url_image] =  URL_BASE + html_doc.at_css("div#trashie img#proddetailimg")['src']

      @meta[:name] = html_doc.at_css("div#trashie div.description h6").content.gsub(/\r\n/,'').strip

      @meta[:item] = html_doc.at_css("div#trashie div.description span.item-no").content.gsub(/\r\n\s+#/,'').strip
      @meta[:item] = @meta[:id].to_s if @meta[:item] =~ /N\/A/ # 167 - 175 had N/A
      @meta[:item_num] = @meta[:item].to_i

      @meta[:url_image_name] = File.basename(@meta[:url_image])
      @image = open(@meta[:url_image]).read

      html_doc.at_css("div#trashie div.description p").to_s.gsub(/\r\n/,'').strip.split("<br>").each do |br|
        next if br.empty?
        next if br =~ /iframe/
        br.gsub!(/^<p>\s+/,'')
        br =~ /<b>(.*):.*<\/b>(.*)/
        key = $1.downcase
        val = $2.strip
        case key
        when /series/ 
          @is_series = val =~ /Series/
          @meta[:series] = val
          @meta[:series_num] = val.gsub(/Series /,'').to_i
        when /team/ then @meta[:team] = val
        when /finish/ then @meta[:finish] = val
        when /rarity/ then @meta[:rarity] = val
        end
      end
    rescue 
      puts "parsing fail on #{@meta[:url_product]}"
    end

    @meta[:imagefile] = "Series#{@meta[:series_num]}_#{@meta[:item]}_#{@meta[:url_image_name]}".downcase
    @meta[:imagepath] = "#{save_path}/#{@meta[:imagefile]}"
  end

  def is_series?
    @is_series
  end

  def save_image!
    if @is_series 
      png_fname = @meta[:imagepath]
      # txt_fname = png_fname.gsub(/png/,'txt')
      begin 
        File.open(png_fname, "w") do |f|
          f.write @image
        end
        # File.open(txt_fname, "w") do |f|
        #   f.write to_json
        # end
        puts self
      rescue StandardError => e
        puts e
      end
    else
      puts "#{@meta[:id]} Not A Series, Not Saving"
    end
  end

  def to_s
    to_json
  end

  def to_json
    JSON.pretty_generate(@meta)
  end
end

if $0 == __FILE__ 
  (1..10).each do |id|
    trashie = Trashie.new(id)
    trashie.save_image!
  end
end

__END__
<div id="trashie" class="trashie-item">&#13;
                <img id="proddetailimg" src="/media/5621/Awful-pie.png" alt="" class="preview" height="192" width="185" style="padding-right: 20px;"/><div class="description">&#13;
                    <h6>&#13;
                        Awful Pie</h6>&#13;
                    <br/><span class="item-no">&#13;
                        # 001</span>&#13;
                    <p>&#13;
                        <b>Series: </b>Series 1<br/><b>Team: </b>The Grubz<br/><b>Finish: </b>Classic<br/><b>Rarity: </b>Common <br/><br/><iframe src="//www.facebook.com/plugins/like.php?href=collect.trashpack.com/product-detail.aspx%3Fid%3D1&amp;send=false&amp;layout=button_count&amp;width=150&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:150px; height:21px;" allowtransparency="true"/>&#13;
                    </p>&#13;
                </div>&#13;
                <div class="clear">&#13;
                     </div>&#13;
                <ul class="buttons"><li><a runat="server" clientidmode="static" class="btn_want" title="I Want" style="background: transparent url('/media/369/btn_product_want-large.png') no-repeat;" href="/base/mooseHelper/RefreshWantProductItem/1">I Want</a></li>&#13;
                    <li><a runat="server" clientidmode="static" class="btn_own" title="I Own" style="background: url(/media/359/btn_product_own-large.png);" href="/base/mooseHelper/RefreshOwnProductItem/1">I Own</a></li>                   &#13;
                </ul><ul class="collections"><li>Foil Bag</li><li>2 Pack</li><li>5 Pack</li><li>12 Pack</li>&#13;
                </ul><div class="clear">&#13;
                     </div>&#13;
            </div>


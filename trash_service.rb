#!/usr/bin/env ruby

require 'sinatra/base'
require 'json'

require './lib/trash_picker'

class TrashService < Sinatra::Base

  configure do
    puts settings.root
    $trash_picker ||= TrashPicker.new
  end

  ['/', '/own'].each do |path|
    get path do
      @title = "Matthew's #{$trash_picker.own_count} Trashies"
      @pile = $trash_picker.own
      haml :index
    end
  end

  get '/all' do
    @title = "All #{$trash_picker.all_count} Trashies"
    @pile = $trash_picker.all
    haml :index
  end

  ['/json', '/own.json'].each do |path|
    get path do
      content_type :json
      JSON.pretty_generate($trash_picker.own)
    end
  end

  get '/all.json' do
    content_type :json
    JSON.pretty_generate($trash_picker.all)
  end

end

